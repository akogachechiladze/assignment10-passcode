package com.example.assignment10passcode.ui

import androidx.core.view.forEach
import androidx.recyclerview.widget.GridLayoutManager
import com.example.assignment10passcode.ItemOnClick
import com.example.assignment10passcode.R
import com.example.assignment10passcode.adapter.RecyclerViewAdapter
import com.example.assignment10passcode.databinding.PasscodeFragmentBinding
import com.example.assignment10passcode.model.buttonModel

class PasscodeFragment : BaseFragment<PasscodeFragmentBinding>(PasscodeFragmentBinding::inflate) {

    private lateinit var adapter: RecyclerViewAdapter

    private val items = mutableListOf<buttonModel>()
    private var passcode = mutableListOf<String>()

    override fun start() {
        fillButtonList()
        initRecyclerView()
    }

    private fun initRecyclerView() {
        binding.recyclerViewForNumbers.layoutManager = GridLayoutManager(requireContext(), 3)
        adapter = RecyclerViewAdapter(items, object : ItemOnClick{
            override fun itemOnClick(position: Int, number: String?, src: Int?) {
                if (number == null && src == R.drawable.ic_delete) {
                    if (passcode.isNotEmpty()){
                        passcode.removeAt(passcode.size - 1)
                        passcodeDelete()
                }
                }else if (number != null) {
                    if (passcode.size < 4) {
                        passcode.add(number)
                        passcodeWrite()
                    }
                }
            }
        })
        binding.recyclerViewForNumbers.adapter = adapter
    }

    private fun passcodeDelete() {
        binding.circles.forEach {
            it.setBackgroundResource(R.drawable.before_click_circle)
        }
    }

    private fun passcodeWrite() {
        binding.circles.forEach {
            it.setBackgroundResource(R.drawable.after_click_circle)
        }
    }


    private fun fillButtonList() {
        items.add(buttonModel(1, null))
        items.add(buttonModel(2, null))
        items.add(buttonModel(3, null))
        items.add(buttonModel(4, null))
        items.add(buttonModel(5, null))
        items.add(buttonModel(6, null))
        items.add(buttonModel(7, null))
        items.add(buttonModel(8, null))
        items.add(buttonModel(9, null))
        items.add(buttonModel(null, R.drawable.ic_touch__id))
        items.add(buttonModel(0, null))
        items.add(buttonModel(null, R.drawable.ic_delete))
    }


}
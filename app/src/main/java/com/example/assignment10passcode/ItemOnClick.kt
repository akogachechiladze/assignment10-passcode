package com.example.assignment10passcode

interface ItemOnClick {
    fun itemOnClick(position: Int, number: String?, src: Int? = null)
}
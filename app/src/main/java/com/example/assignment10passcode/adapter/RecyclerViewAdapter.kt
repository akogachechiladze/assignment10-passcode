package com.example.assignment10passcode.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.assignment10passcode.ItemOnClick
import com.example.assignment10passcode.databinding.NumberItemLayoutBinding
import com.example.assignment10passcode.databinding.SrcItemLayoutBinding
import com.example.assignment10passcode.model.buttonModel


//albat/vimedovneb musha recycler
class RecyclerViewAdapter(private val items: List<buttonModel>, private val onItemListener: ItemOnClick): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object{
        private const val NUMBER = 1
        private const val SRC = 2
    }




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == NUMBER){
            numberViewHolder(NumberItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }else {
            srcViewHolder(SrcItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is numberViewHolder -> holder.bindNumber()
            is srcViewHolder -> holder.bindSrc()
        }
    }

    override fun getItemCount() = items.size


    inner class srcViewHolder(private val binding: SrcItemLayoutBinding): RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        private lateinit var item: buttonModel
        fun bindSrc() {
            item = items[adapterPosition]
            item.src?.let { binding.srcImgBtn.setImageResource(it) }
        }
        override fun onClick(p0: View?) {
            onItemListener.itemOnClick(adapterPosition, null , item.src)
        }

    }

    inner class numberViewHolder(private val binding: NumberItemLayoutBinding): RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        private lateinit var item:buttonModel
        fun bindNumber() {
            item = items[adapterPosition]
            binding.numberBtn.text = item.number.toString()
        }

        override fun onClick(p0: View?) {
            onItemListener.itemOnClick(adapterPosition,item.number.toString())
        }

    }

    override fun getItemViewType(position: Int): Int {
        val item = items[position]
        return if(item.src == null) {
            NUMBER
        }else {
            SRC
        }
    }

}